class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|

      t.boolean :group_type, null:false
      t.string :group_name, null:false
      t.string :description, null:false
      t.string :creator_name
      t.string :cover

      t.timestamps
    end
  end
end
