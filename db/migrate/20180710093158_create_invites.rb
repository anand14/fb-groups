class CreateInvites < ActiveRecord::Migration[5.2]
  def change
    create_table :invites do |t|

      t.references :group, null:false
      t.references :user, null:false
      t.boolean :invite_status, null:false
      t.timestamp :time, null:false
      t.timestamps

    end
  end
end
