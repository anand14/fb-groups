class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      
      t.string :username, null: false  
      t.string :email, null: false  
      t.string :big_image
      t.string :small_image
      t.boolean :admin
      t.string :first_name, null: false
      t.string :last_name, null: false

      t.timestamps
    end
  end
end
